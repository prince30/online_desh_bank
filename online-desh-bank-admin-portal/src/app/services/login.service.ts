import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
//import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor (private http: Http) {}

  sendCredential(username: string, password: string) {
    let url = "http://localhost:9090/online_desh_bank/index";
    let params = 'username='+username+'&password='+password;
    let headers = new Headers(
    {
      'Content-Type': 'application/x-www-form-urlencoded'
      // 'Access-Control-Allow-Credentials' : true
    });
    return this.http.post(url, params, {headers: headers, withCredentials : true});
  }

  logout() {
     let url = "http://localhost:9090/online_desh_bank/logout";
     return this.http.get(url, { withCredentials: true });
   }

  
}
