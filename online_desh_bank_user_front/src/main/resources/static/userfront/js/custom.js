$(document).ready(function() {
	var confirm = function() {
		bootbox.confirm({
			title : "Appointment Confirmation",
			message : "Do you really want to schedule this appointment?",
			buttons : {
				cancel : {
					label : '<i class="fa fa-times"></i> Cancel'
				},
				confirm : {
					label : '<i class="fa fa-check"></i> Confirm'
				}
			},
			callback : function(result) {
				if (result == true) {
					$('#appointmentForm').submit();
				} else {
					console.log("Scheduling cancelled.");
				}
			}
		});
	};

	$('#datetimepicker1').datetimepicker({
		"format" : "DD-MM-YYYY hh:mm:ss A",
	});

	$('#submitAppointment').click(function() {
		confirm();
	});
	
	$('#example').DataTable();

});
