package com.prince.online_desh_bank_user_front.service;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.entity.security.UserRole;
import com.prince.online_desh_bank_user_front.repos.RoleDao;
import com.prince.online_desh_bank_user_front.repos.UserDao;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private static final Logger lOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private RoleDao roleDao;

	@Autowired
	private AccountService accountService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public User findByEmail(String email) {
		return userDao.findByEmail(email);
	}

	@Override
	public boolean checkUserExists(String username, String email) {

		if (checkUsernameExists(username) || checkEmailExists(email)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean checkUsernameExists(String username) {
		if (null != findByUsername(username)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean checkEmailExists(String email) {
		if (null != findByEmail(email)) {
			return true;
		}
		return false;
	}

	@Override
	public void save(User user) {
		userDao.save(user);

	}

	@Override
	public User createUser(User user, Set<UserRole> userRole) {

		User localUser = userDao.findByUsername(user.getUsername());
		if (localUser != null) {
			lOGGER.warn("User with username {} already exists. Nothing will be done.", user.getUsername());
		} else {
			String encryptPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(encryptPassword);

			for (UserRole ur : userRole) {
				roleDao.save(ur.getRole());
			}
			user.getUserRoles().addAll(userRole);

			user.setPrimaryAccount(accountService.createPrimaryAccount());
			user.setSavingsAccount(accountService.createSavingsAccount());

			localUser = userDao.save(user);
		}
		return localUser;
	}

	@Override
	public User updateUser(User user) {
		return userDao.save(user);

	}

	@Override
	public List<User> findUserList() {
		return userDao.findAll();
	}
	
	public void enableUser(String username) {
		User user = findByUsername(username);
		user.setEnabled(true);
		userDao.save(user);
	}

	public void disableUser(String username) {
		User user = findByUsername(username);
		user.setEnabled(false);
		System.out.println(user.isEnabled());
		userDao.save(user);
		System.out.println(username + " is disabled.");
	}
}
