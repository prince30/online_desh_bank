package com.prince.online_desh_bank_user_front.service;

import java.util.List;
import java.util.Set;

import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.entity.security.UserRole;


public interface UserService {
	User findByUsername(String username);

	User findByEmail(String email);

	boolean checkUserExists(String username, String email);

	boolean checkUsernameExists(String username);

	boolean checkEmailExists(String email);

	void save(User user);

	User createUser(User user, Set<UserRole> userRole);

	User updateUser(User user);

	List<User> findUserList();
	
    void enableUser (String username);

    void disableUser (String username);

}
