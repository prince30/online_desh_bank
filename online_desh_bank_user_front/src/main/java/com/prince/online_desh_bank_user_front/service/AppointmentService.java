package com.prince.online_desh_bank_user_front.service;

import java.util.List;

import com.prince.online_desh_bank_user_front.entity.Appointment;


public interface AppointmentService {
	Appointment createAppointment(Appointment appointment);

    List<Appointment> findAll();

    Appointment findAppointment(Long id);

    void confirmAppointment(Long id);
}
