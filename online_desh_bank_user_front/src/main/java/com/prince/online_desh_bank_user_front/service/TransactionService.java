package com.prince.online_desh_bank_user_front.service;

import java.security.Principal;
import java.util.List;

import com.prince.online_desh_bank_user_front.entity.PrimaryAccount;
import com.prince.online_desh_bank_user_front.entity.PrimaryTransaction;
import com.prince.online_desh_bank_user_front.entity.Recipient;
import com.prince.online_desh_bank_user_front.entity.SavingsAccount;
import com.prince.online_desh_bank_user_front.entity.SavingsTransaction;

public interface TransactionService {
	List<PrimaryTransaction> findPrimaryTransactionList(String username);

	List<SavingsTransaction> findSavingsTransactionList(String username);

	void savePrimaryDepositTransaction(PrimaryTransaction primaryTransaction);

	void saveSavingsDepositTransaction(SavingsTransaction savingsTransaction);

	void savePrimaryWithdrawTransaction(PrimaryTransaction primaryTransaction);

	void saveSavingsWithdrawTransaction(SavingsTransaction savingsTransaction);

	void betweenAccountsTransfer(String transferFrom, String transferTo, String amount, PrimaryAccount primaryAccount,
			SavingsAccount savingsAccount) throws Exception;

	List<Recipient> findRecipientList(Principal principal);

	void saveRecipient(Recipient recipient);

	void deleteRecipientById(Long recipientId);

	Recipient findRecipientByName(String recipientName);

	void toSomeoneElseTransfer(Recipient recipient, String accountType, String amount, PrimaryAccount primaryAccount,
			SavingsAccount savingsAccount);
}
