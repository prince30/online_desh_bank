package com.prince.online_desh_bank_user_front.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.SavingsTransaction;

public interface SavingsTransactionDao extends CrudRepository<SavingsTransaction, Long> {
	//List<SavingsTransaction> finfAll();
}
