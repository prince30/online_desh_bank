package com.prince.online_desh_bank_user_front.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.Appointment;


public interface AppointmentDao extends CrudRepository<Appointment, Long> {

    List<Appointment> findAll();
    
    //Appointment findAppointment(Long appointmentId);
}
