package com.prince.online_desh_bank_user_front.repos;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.SavingsAccount;

public interface SavingsAccountDao extends CrudRepository<SavingsAccount, Long> {

	SavingsAccount findByAccountNumber(int accountNumber);

}
