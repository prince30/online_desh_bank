package com.prince.online_desh_bank_user_front.repos;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.security.Role;

public interface RoleDao extends CrudRepository<Role, Long> {
	Role findByName(String name);
}
