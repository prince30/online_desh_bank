package com.prince.online_desh_bank_user_front.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.PrimaryTransaction;
import com.prince.online_desh_bank_user_front.entity.SavingsTransaction;

public interface PrimaryTransactionDao extends CrudRepository<PrimaryTransaction, Long> {
	//List<PrimaryTransaction> finfAll();
}
