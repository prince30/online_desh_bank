package com.prince.online_desh_bank_user_front.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.Recipient;

public interface RecipientDao extends CrudRepository<Recipient, Long> {
	List<Recipient> findAll();

	Recipient findByName(String recipientName);

	void deleteById(Long recipientId);

}
