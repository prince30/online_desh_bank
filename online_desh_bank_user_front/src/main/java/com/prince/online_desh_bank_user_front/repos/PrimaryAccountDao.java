package com.prince.online_desh_bank_user_front.repos;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.PrimaryAccount;

public interface PrimaryAccountDao extends CrudRepository<PrimaryAccount, Long> {

	PrimaryAccount findByAccountNumber(int accountNumber);

}
