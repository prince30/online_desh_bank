package com.prince.online_desh_bank_user_front.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.prince.online_desh_bank_user_front.entity.User;

public interface UserDao extends CrudRepository<User, Long> {
	User findByUsername(String username);

	User findByEmail(String email);
	
	List<User> findAll();
}
