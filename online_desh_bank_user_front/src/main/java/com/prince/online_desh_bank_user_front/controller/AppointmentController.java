package com.prince.online_desh_bank_user_front.controller;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prince.online_desh_bank_user_front.entity.Appointment;
import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.service.AppointmentService;
import com.prince.online_desh_bank_user_front.service.UserService;

@Controller
//@RequestMapping("/appointment")
public class AppointmentController {

	private static final Logger lOGGER = LoggerFactory.getLogger(AppointmentController.class);

	@Autowired
	private AppointmentService appointmentService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/appointmentCreate", method = RequestMethod.GET)
	public String createAppointment(ModelMap modelMap) {
		Appointment appointment = new Appointment();

		modelMap.addAttribute("appointment", appointment);
		modelMap.addAttribute("dateString", "");

		return "userfront/appointment";
	}

	@RequestMapping(value = "/appointmentCreate", method = RequestMethod.POST)
	public String createAppointmentPost(@ModelAttribute("appointment") Appointment appointment,
			@ModelAttribute("dateString") String date, Principal principal) throws ParseException {

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date d1 = format1.parse(date);
		appointment.setDate(d1);

		User user = userService.findByUsername(principal.getName());
		appointment.setUser(user);

		appointmentService.createAppointment(appointment);

		return "userfront/appointment";
	}

}
