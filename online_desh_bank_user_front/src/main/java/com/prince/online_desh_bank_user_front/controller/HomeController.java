package com.prince.online_desh_bank_user_front.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prince.online_desh_bank_user_front.entity.PrimaryAccount;
import com.prince.online_desh_bank_user_front.entity.SavingsAccount;
import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.entity.security.UserRole;
import com.prince.online_desh_bank_user_front.repos.RoleDao;
import com.prince.online_desh_bank_user_front.service.UserService;

@Controller
public class HomeController {

	private static final Logger lOGGER = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private RoleDao roleDao;

	@RequestMapping("/")
	public String home() {
		return "redirect:/index";
	}

	@RequestMapping("/index")
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showAccountFrom(ModelMap modelMap) {
		User user = new User();
		modelMap.addAttribute("user", user);
		return "register";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(@ModelAttribute("user") User user, ModelMap modelMap) {

		if (userService.checkUserExists(user.getUsername(), user.getEmail())) {
			if (userService.checkEmailExists(user.getEmail())) {
				modelMap.addAttribute("emailExists", true);
			}
			if (userService.checkUsernameExists(user.getUsername())) {
				modelMap.addAttribute("usernameExists", true);
			}

			return "register";
		} else {
			
			Set<UserRole> userRole = new HashSet<>();
			userRole.add(new UserRole(user, roleDao.findByName("ROLE_USER")));
			userService.createUser(user, userRole);
			return "redirect:/";
		}

	}

	@RequestMapping("/userfront")
	public String userFront(Principal principal, ModelMap modelMap) {
		User user = userService.findByUsername(principal.getName());
		PrimaryAccount primaryAccount = user.getPrimaryAccount();
		SavingsAccount savingsAccount = user.getSavingsAccount();

		modelMap.addAttribute("primaryAccount", primaryAccount);
		modelMap.addAttribute("savingsAccount", savingsAccount);

		return "userfront/home";
	}

}
