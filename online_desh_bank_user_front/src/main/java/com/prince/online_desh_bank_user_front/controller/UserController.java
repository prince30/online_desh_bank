package com.prince.online_desh_bank_user_front.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.service.UserService;

@Controller
//@RequestMapping("/user")
public class UserController {

	private static final Logger lOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String profile(Principal principal, ModelMap modelMap) {
		User user = userService.findByUsername(principal.getName());
		modelMap.addAttribute("user", user);
		return "userfront/profile";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public String profileUpdate(@ModelAttribute("user") User newUser, Principal principal, ModelMap modelMap) {
		User user = userService.findByUsername(principal.getName());

		user.setUsername(newUser.getUsername());
		user.setFirstName(newUser.getFirstName());
		user.setLastName(newUser.getLastName());
		user.setPhone(newUser.getPhone());
		user.setEmail(newUser.getEmail());

		modelMap.addAttribute("user", user);

		userService.updateUser(user);

		return "userfront/profile";
	}
}
