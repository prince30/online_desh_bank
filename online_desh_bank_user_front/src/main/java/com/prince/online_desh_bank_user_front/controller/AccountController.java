package com.prince.online_desh_bank_user_front.controller;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.prince.online_desh_bank_user_front.entity.PrimaryAccount;
import com.prince.online_desh_bank_user_front.entity.PrimaryTransaction;
import com.prince.online_desh_bank_user_front.entity.SavingsAccount;
import com.prince.online_desh_bank_user_front.entity.SavingsTransaction;
import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.service.AccountService;
import com.prince.online_desh_bank_user_front.service.TransactionService;
import com.prince.online_desh_bank_user_front.service.UserService;

@Controller
//@RequestMapping("/account")
public class AccountController {

	private static final Logger lOGGER = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionService transactionService;

	@RequestMapping("/primaryAccount")
	public String primaryAccount(Principal principal, ModelMap modelMap) {
		List<PrimaryTransaction> primaryTransactionList = transactionService
				.findPrimaryTransactionList(principal.getName());

		User user = userService.findByUsername(principal.getName());
		PrimaryAccount primaryAccount = user.getPrimaryAccount();
		modelMap.addAttribute("primaryAccount", primaryAccount);
		modelMap.addAttribute("primaryTransactionList", primaryTransactionList);

		return "userfront/primary_account";
	}

	@RequestMapping("/savingsAccount")
	public String savingsAccount(Principal principal, ModelMap modelMap) {
		List<SavingsTransaction> savingsTransactionList = transactionService
				.findSavingsTransactionList(principal.getName());

		User user = userService.findByUsername(principal.getName());
		SavingsAccount savingsAccount = user.getSavingsAccount();
		modelMap.addAttribute("savingsAccount", savingsAccount);
		modelMap.addAttribute("savingsTransactionList", savingsTransactionList);
		return "userfront/savings_account";
	}

	@RequestMapping(value = "/deposit", method = RequestMethod.GET)
	public String deposit(ModelMap modelMap) {
		modelMap.addAttribute("accountType", "");
		modelMap.addAttribute("amount", "");
		return "userfront/deposit";
	}

	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	public String depositAdd(@ModelAttribute("amount") String amount, @ModelAttribute("accountType") String accountType,
			Principal principal) {
		accountService.deposit(accountType, Double.parseDouble(amount), principal);
		// return "userfront/primary_account";
		// return "userfront/home";
		return "redirect:/userfront";
	}

	@RequestMapping(value = "/withdraw", method = RequestMethod.GET)
	public String withdraw(ModelMap modelMap) {
		modelMap.addAttribute("accountType", "");
		modelMap.addAttribute("amount", "");
		return "userfront/withdraw";
	}

	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	public String withdrawAdd(@ModelAttribute("amount") String amount,
			@ModelAttribute("accountType") String accountType, Principal principal) {
		accountService.withdraw(accountType, Double.parseDouble(amount), principal);
		// return "userfront/primary_account";
		// return "userfront/home";
		return "redirect:/userfront";
	}

}
