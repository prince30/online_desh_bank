package com.prince.online_desh_bank_user_front.controller;

import java.security.Principal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.prince.online_desh_bank_user_front.entity.PrimaryAccount;
import com.prince.online_desh_bank_user_front.entity.Recipient;
import com.prince.online_desh_bank_user_front.entity.SavingsAccount;
import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.service.TransactionService;
import com.prince.online_desh_bank_user_front.service.UserService;

@Controller
//@RequestMapping("/transfer")
public class TransferController {

	private static final Logger lOGGER = LoggerFactory.getLogger(TransferController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private TransactionService transactionService;

	@RequestMapping(value = "/betweenAccounts", method = RequestMethod.GET)
	public String betweenAccounts(ModelMap modelMap) {
		modelMap.addAttribute("transferFrom", "");
		modelMap.addAttribute("transferTo", "");
		modelMap.addAttribute("amount", "");
		return "userfront/betweenAccounts";
	}

	@RequestMapping(value = "/betweenAccounts", method = RequestMethod.POST)
	public String betweenAccountsPost(@ModelAttribute("transferFrom") String transferFrom,
			@ModelAttribute("transferTo") String transferTo, @ModelAttribute("amount") String amount,
			Principal principal) throws Exception {
		User user = userService.findByUsername(principal.getName());
		PrimaryAccount primaryAccount = user.getPrimaryAccount();
		SavingsAccount savingsAccount = user.getSavingsAccount();
		transactionService.betweenAccountsTransfer(transferFrom, transferTo, amount, primaryAccount, savingsAccount);

		return "userfront/betweenAccounts";
	}

	@RequestMapping(value = "/recipient", method = RequestMethod.GET)
	public String showRecipientForm(ModelMap modelMap, Principal principal) {

		List<Recipient> recipientList = transactionService.findRecipientList(principal);
		Recipient recipient = new Recipient();
		modelMap.addAttribute("recipientList", recipientList);
		modelMap.addAttribute("recipient", recipient);

		return "userfront/recipients";
	}

	@RequestMapping(value = "/recipient/save", method = RequestMethod.POST)
	public String recipientPost(@ModelAttribute("recipient") Recipient recipient, Principal principal) {

		User user = userService.findByUsername(principal.getName());
		recipient.setUser(user);
		transactionService.saveRecipient(recipient);

		return "userfront/recipients";
	}

	@RequestMapping(value = "/recipient/edit", method = RequestMethod.GET)
	public String recipientEdit(@RequestParam(value = "recipientName") String recipientName, Principal principal,
			ModelMap modelMap) {

		Recipient recipient = transactionService.findRecipientByName(recipientName);

		List<Recipient> recipientList = transactionService.findRecipientList(principal);

		modelMap.addAttribute("recipient", recipient);
		modelMap.addAttribute("recipientList", recipientList);

		return "userfront/recipients";
	}

	@RequestMapping(value = "/recipient/delete", method = RequestMethod.GET)
	@Transactional
	public String deleteRecipient(@RequestParam("recipientId") Long recipientId, Principal principal,
			ModelMap modelMap) {

		transactionService.deleteRecipientById(recipientId);
		List<Recipient> recipientList = transactionService.findRecipientList(principal);

		Recipient recipient = new Recipient();

		modelMap.addAttribute("recipient", recipient);
		modelMap.addAttribute("recipientList", recipientList);

		return "userfront/recipients";
	}

	@RequestMapping(value = "/toSomeoneElse", method = RequestMethod.GET)
	public String toSomeoneElseFrom(Principal principal, ModelMap modelMap) {
		List<Recipient> recipientList = transactionService.findRecipientList(principal);

		modelMap.addAttribute("accountType", "");
		modelMap.addAttribute("recipientList", recipientList);

		return "userfront/toSomeoneElse";
	}

	@RequestMapping(value = "/toSomeoneElse", method = RequestMethod.POST)
	public String toSomeoneElsePost(@ModelAttribute("recipientName") String recipientName,
			@ModelAttribute("accountType") String accountType, @ModelAttribute("amount") String amount,
			Principal principal) {

		User user = userService.findByUsername(principal.getName());
		Recipient recipient = transactionService.findRecipientByName(recipientName);

		PrimaryAccount primaryAccount = user.getPrimaryAccount();
		SavingsAccount savingsAccount = user.getSavingsAccount();

		transactionService.toSomeoneElseTransfer(recipient, accountType, amount, primaryAccount, savingsAccount);

		return "userfront/toSomeoneElse";
	}
}
