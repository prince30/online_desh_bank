package com.prince.online_desh_bank_user_front.entity.security;

import org.springframework.security.core.GrantedAuthority;

public class Authority implements GrantedAuthority{

    private final String authority;

    public Authority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}
