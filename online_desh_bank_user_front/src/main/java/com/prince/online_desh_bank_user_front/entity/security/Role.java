package com.prince.online_desh_bank_user_front.entity.security;

import javax.persistence.*;

import com.prince.online_desh_bank_user_front.entity.AbstractEntity;

import java.util.HashSet;
import java.util.Set;



@Entity
@Table(name = "roles")
public class Role extends AbstractEntity {

    private String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<UserRole> userRoles = new HashSet<>();

    public Role() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }


}
