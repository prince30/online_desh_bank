package com.prince.online_desh_bank_user_front.entity.security;


import javax.persistence.*;

import com.prince.online_desh_bank_user_front.entity.AbstractEntity;
import com.prince.online_desh_bank_user_front.entity.User;


@Entity
@Table(name = "user_roles")
public class UserRole extends AbstractEntity {

    public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    public UserRole() {}

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }


}
