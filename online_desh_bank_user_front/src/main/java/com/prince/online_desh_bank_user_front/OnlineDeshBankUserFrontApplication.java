package com.prince.online_desh_bank_user_front;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineDeshBankUserFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineDeshBankUserFrontApplication.class, args);
	}
}
