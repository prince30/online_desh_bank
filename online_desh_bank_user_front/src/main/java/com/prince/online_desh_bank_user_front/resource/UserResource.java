package com.prince.online_desh_bank_user_front.resource;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prince.online_desh_bank_user_front.entity.PrimaryTransaction;
import com.prince.online_desh_bank_user_front.entity.SavingsTransaction;
import com.prince.online_desh_bank_user_front.entity.User;
import com.prince.online_desh_bank_user_front.service.TransactionService;
import com.prince.online_desh_bank_user_front.service.UserService;


@RestController
@RequestMapping("/api")
@PreAuthorize("hasRole('ADMIN')")
public class UserResource {

	private static final Logger lOGGER = LoggerFactory.getLogger(UserResource.class);
	
	@Autowired
	private UserService userService;

	@Autowired
	private TransactionService transactionService;
	
	
	@RequestMapping(value = "/user/all", method = RequestMethod.GET)
	public List<User> userList(){
		return userService.findUserList();
	}
	
	
	@RequestMapping(value = "/user/primary/transaction", method = RequestMethod.GET)
    public List<PrimaryTransaction> getPrimaryTransactionList(@RequestParam("username") String username) {
        return transactionService.findPrimaryTransactionList(username);
    }

    @RequestMapping(value = "/user/savings/transaction", method = RequestMethod.GET)
    public List<SavingsTransaction> getSavingsTransactionList(@RequestParam("username") String username) {
        return transactionService.findSavingsTransactionList(username);
    }

    @RequestMapping("/user/{username}/enable")
    public void enableUser(@PathVariable("username") String username) {
        userService.enableUser(username);
    }

    @RequestMapping("/user/{username}/disable")
    public void diableUser(@PathVariable("username") String username) {
        userService.disableUser(username);
    }
	
	
	
}
