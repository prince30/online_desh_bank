package com.prince.online_desh_bank_user_front;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OnlineDeshBankUserFrontApplicationTests {

	@Test
	public void contextLoads() {
	}

}
